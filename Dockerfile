FROM mazurov/cern-root:6.08.00
MAINTAINER alexander.mazurov <alexander.mazurov@gmail.com>

LABEL description="CERN ROOT framework"

RUN yum -y install cmake3 make gsl-devel libSM libX11 libXft libXpm libXext && yum -y clean all
RUN ln -s /usr/bin/cmake3 /usr/bin/cmake
# Set ROOT environment
ENV ROOTSYS         "/opt/root"
ENV PATH            "$ROOTSYS/bin:$ROOTSYS/bin/bin:$PATH"
ENV LD_LIBRARY_PATH "$ROOTSYS/lib:$LD_LIBRARY_PATH"
ENV PYTHONPATH      "$ROOTSYS/lib:PYTHONPATH"

RUN yum -y install python3-devel
RUN pip3 install --upgrade pip
##RUN yum -y install python-pip && yum clean all
#ARG NB_USER
#ARG NB_UID
#ENV USER ${NB_USER}
#ENV HOME /tmp #/home/${NB_USER}

#RUN adduser --disabled-password \
#    --gecos "Default user" \
#    --uid ${NB_UID} \
#    ${NB_USER}
#WORKDIR ${HOME}

RUN pip3 install jupyter
#RUN pip3 install jupyterlab
#RUN pip3 install matplotlib
#RUN pip3 install numpy
#FROM python:3.7-slim
#RUN pip install --no-cache notebook

#COPY . /

#EXPOSE 8888

#ENTRYPOINT ["jupyter", "lab","--ip=0.0.0.0","--allow-root", "--notebook-dir=/"]
